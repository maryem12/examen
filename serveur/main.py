from typing import Optional
from fastapi import FastAPI,Path
import requests
import json
import uvicorn
from dotenv import load_dotenv
import os
import random
load_dotenv()
Audio_DB = os.environ.get("Audio_DB")
searchArtist = "search.php?s="
searchAlbum= "album.php?i="
SearchTrack = "track.php?m="
Lyricsovh  = os.environ.get("Lyrics")
app = FastAPI()

@app.get("/")
def HOME():
    """
    teste la bonne configuration et la disponibilité des services dont dépend l'API 
    
   Returns:
   dictionnaire qui contien les status de serveur
    """
    url_AudioDB=Audio_DB + searchArtist + "rick astley"
    rAudioDB = requests.get(url_AudioDB)
    url_Lyrics=Lyricsovh + "Rick Astley" + "/" + "Never Gonna Give You Up"
    rLyrics = requests.get(url_Lyrics)
   
    return {"AudioDB_status_code":rAudioDB.status_code,"Lyricsovh_status_code": rLyrics.status_code}

def get_id_artist_by_name(artist_name : str):
    """methode  qui retourne id de l'artiste a travers son non

    Args:
        artist_name (str): le nom de l'artiste

    Returns:
        _type_: id de l'artiste
    """
    try:
           url=Audio_DB+searchArtist
           r=requests.get(url+artist_name)
           data = r.json()
           id_artist=data['artists'][0]['idArtist']
           return id_artist
    except:
          return("ArtistNotFound")
      
      
      
def get_album_artist_by_id(id_artist:int):
    """
     methode  qui retourne id de l'album d'un artiste  a travers son id
    Args:
        id_artist (int): l'id de l'atiste

    Returns:
        _type_: id artiste
    """
    try:
           url=Audio_DB+searchAlbum
           r=requests.get(url+str(id_artist))
           data = r.json()
           id_album=data['album'][0]['idAlbum']
           return id_album
    except:
        return("AlbumNotFound") 

def get_track_artist_by_id_album(id_album:int):
    """methode qui génére les infomations sur la chanson d'un artiste

    Args:
        id_album (int): id_album

    Returns:
        dictionnaire qui contient les information 
      
    """
    try:
           url=Audio_DB+SearchTrack
           r=requests.get(url+str(id_album))
           data = r.json()
           tracks=data['track']
           track = random.choice(tracks)
           strArtist = track["strArtist"]
           title = track["strTrack"]
           if track["strMusicVid"]:
                suggested_youtube_url = track["strMusicVid"]
           else:
               #si url youtube n'est pas dispo
                suggested_youtube_url="lien non disponible"
           return {"artist" : strArtist, "title" : title, "suggested_youtube_url" : suggested_youtube_url}

           
    except:
        return("TrackNotFound") 
  
def get_lyrics(artist_name :str,title : str):
    """methode qui retourne lyrics d'une chanson donnee 

    Args:
        artist_name (str): nom de l'artiste
        title (str): titre du chanson

    Returns:
        _type_: dictionnaire
    """
    try:
        url=Lyricsovh + artist_name + "/" + title
        r=requests.get(url) 
        data=r.json()
        return data["lyrics"]
    except:
        return("lyricsNotFound")
 
@app.get("/random/{artist_name}")
def get_music_information(artist_name:str= Path(None,description="the name of the artist")):
         id_artist=get_id_artist_by_name(artist_name)
         if id_artist!="ArtistNotFound":
            id_album=get_album_artist_by_id(id_artist)
            if id_album!="AlbumNotFound":
               track=get_track_artist_by_id_album(id_album)
               lyrics=get_lyrics(artist_name,track["title"])
               if lyrics!="lyricsNotFound":
                    track["lyrics"]=lyrics
               else:
                   track["lyrics"]="lyrics non disponibles"
               return track
            else:
                return id_album
         else:
             return id_artist
             
           
                

if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000)
