
import json
import random
import os
import requests
from dotenv import load_dotenv


load_dotenv()
APP_Music=os.environ.get("APP_Music")
with open("rudy.json", encoding="utf8")  as f:
    data = json.load(f)


def check_disp():
    #pour tester le bon fonctionnment du APP music
    r= requests.get(APP_Music)
    r.status_code
    return {"APP_Music_status_code":r.status_code}
        
    


def generate_playlist():
    #extraire les artistes 
    artistes=[]
    for j in range(len(data)):
        artistes.append(data[j]["artiste"])
    #extraire les notes d
    notes=[]
    for j in range(len(data)):
         notes.append(data[j]["note"])
    
   
    random.seed(50)
    #random.choices() qui gere une liste des artistes selon leurs notes
    #k=20  pour générer 20 artiste
    gen_artiste = random.choices(artistes, weights=notes,k=20)
    playlist=[]
    i=0
    while i<20:
        play= requests.get(APP_Music+"/random/"+gen_artiste[i]).json()
        while play["lyrics"] =="lyrics non disponibles" or play  in playlist:
            #répéter jusqu'à ce qu'on trouve track avec lyrics et n'existe pas déjà dans la liste des chansons
            play= requests.get(APP_Music+"/random/"+gen_artiste[i]).json()
        i=i+1
        playlist.append(play) 
        
    return playlist
     
 
    

if __name__ == "__main__":
   
   if check_disp()["APP_Music_status_code"]==200:
      plylist=generate_playlist()
      #le path pour enregistrer playlist
      path = "C:/Users/marye/OneDrive/Bureau/playlist"
      file_name= "playlist_rudy"
      with open(f"{path}/{file_name}.json",mode="w") as f:
           json.dump(plylist, f)
   else:
       print("Probleme in APP Music, try again")