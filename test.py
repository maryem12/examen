
from unittest import TestCase
from serveur.main import *

class testServer(TestCase):
    def testGet_id_artist_by_name(self):
        #given
        id_artist="112884"
        #when
        get_id_artist=get_id_artist_by_name("rick astley")
        #then
        self.assertEqual(id_artist,get_id_artist)
    def testGet_album_artist_by_id(self):
        #given
        id_album="2121296"
        #when
        get_id_album=get_album_artist_by_id(112884)
        #then
        self.assertEqual(id_album,get_id_album)
    def testGet_album_artist_by_id_not_ok(self):
        #given
        id_album_f="2121296545555"
        #when
        get_id_album=get_album_artist_by_id(112884)
        #then
        self.assertNotEqual(id_album_f,get_id_album)

