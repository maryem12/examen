# API musicale
Bienvenue dans  l'application “👋“!
### Contexte

L'application a deux objectifs:

D'une part, la réalisation d'une API musicale, qui:

- Permet de proposer de manière aléatoire une chanson, pour un artiste donné en entrée en utilisant deux API AudioDB et LyricsOVH
- Contrôle l'état de santé des api cible.


 D'autre part, mise en place d'un client de ce webservice capable de générer automatiquement une playlist de musiques avec lyrics uniquement composée de musiques de ses artistes préférés.

Dans ce contexte, on a élaboré deux scénarios:

- Scénario/serveur
- Scénario/Client
### Schéma d'architecture

```mermaid
graph TD;
  A[Client]-->|http|B[Serveur];
 B -->|https| C[LyricsOVH];
  B -->|https| D[AudioDB];
```
## Quick start

Pour lancer rapidement l'application veuillez créer deux terminaux :

- Sur le premier, rendez vous à la branche api avec `cd serveur/` et exécutez ce code:

#### installer les dépendances
```
pip install -r requirements.txt
```

#### lancer l'application 
```
python main.py
```

Vérifiez ensuite le fonctionnement de l'api avec ces 2 urls :



-http://localhost:8000 qui teste la bonne configuration et la disponibilité des services dont dépend l'API.


-http://localhost:8000/random/{nomdelartiste} qui renvoie, pour un string correspondant au nom d'un artiste donné en entrée, les informations sur une musique de cet artiste.



- Une fois l'application mise en route, sur le deuxième terminal, rendez vous à la branche client avec `cd Client/` et exécuter le code ci-dessous.

#### Installer les dépendances 

```
pip install -r requirements.txt
```

#### puis pour obtenir une playlist pour Rudy basée sur le fichier rudy.json :

 vous devez aller dans le client/main et changer le chemin dans lequel vous souhaitez enregistrer le playlist .

Puis lancez:
```
python main.py
```
#### Pour tester:
```
python -m unittest test.py
```

